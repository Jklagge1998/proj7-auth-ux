# Laptop Service
from flask import Flask, Flask, jsonify, redirect, g,url_for, request, render_template, abort, Response, json, session
from flask_restful import Resource, Api
from flask_login import UserMixin, login_user, LoginManager, login_required, login_required, current_user, logout_user, confirm_login, fresh_login_required
from flask_wtf.csrf import CSRFProtect
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Length
import flask
from pymongo import MongoClient
import pymongo

import os


from passlib.apps import custom_app_context as pwd_context
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPBasicAuth
from pymongo import MongoClient
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

from werkzeug.utils import redirect


# Instantiate the app
app = Flask(__name__)
api = Api(app)

# Protection Protocol
csrf = CSRFProtect(app)
csrf.init_app(app)
app.config['SECRET_KEY'] = 'NotAGOODSecretKey'
auth = HTTPBasicAuth()

#We'll be using SLALCHEMY and sqlite for our user database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
udb = SQLAlchemy(app)

#for mannaging our logins
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

# Gain acess to mongo db 
client = MongoClient('db', 27017)
db = client.tododb



@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(udb.Model, UserMixin):
    __tablename__ = 'users'
    id = udb.Column(udb.Integer, primary_key=True)
    username = udb.Column(udb.String(12), index=True)
    password_hash = udb.Column(udb.String(64))

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user

@app.route('/')
def index():
	return redirect('/login')

#We'll need two form classes for login and registering (these forms generate html)
class LoginForm(FlaskForm):
    username = StringField('username', validators=[InputRequired('A username is required!'), Length(min=4, max=12)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=4, max=12)])
    remember_me = BooleanField('Remember me')

class RegisterForm(FlaskForm):
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=12)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=4, max=12)])


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return render_template('logined.html')

    form = LoginForm()
    session['next'] = request.args.get('next')
    if form.validate_on_submit():
    	#if submission was valid
        new_user = User.query.filter_by(username=form.username.data).first()
        if new_user:
            if new_user.verify_password(form.password.data):
                flask.flash('Logged in successfully.')
                login_user(new_user, remember=form.remember_me.data) #will store cookie depending on remember me
                return render_template('logined.html')
            else:
                return '<h1>Invalid username or password </h1>'

    return flask.render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return render_template('logout.html')


@auth.verify_password
def verify_password(username_or_token, password):
    # first we will try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        #second we will try to authenticate with username/password
        user = User.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user #global user
    return True



@app.route('/api/register', methods=['POST'])
def register():
	#The goal of this register the user will returning a response
	#as well. 
	if not request.json:
		#If this isn't json than it will most likely be form
		Username = request.form['username']
		Password = request.form['password']
	else:
		Username = request.json['username']
		Password = request.json['password']

	if Username == '' or Password == '':
		abort(400)  # blank password or username
	if User.query.filter_by(username=Username).first() is not None:
	 	abort(400)  # user already exists
	
	user = User(username=Username)
	user.hash_password(Password)#We will hash the password before storing it in db
    
	udb.session.add(user)
	udb.session.flush() 


	JSONuser = ({'username': user.username})
	js = json.dumps(JSONuser)
	resp = Response(js, status=201, mimetype='application/json')
	resp.headers['location'] = '/api/users/{}'.format(user.id)
	udb.session.commit()#finilizes user to db
	return resp
	



@app.route('/register', methods=['GET', 'POST'])
def register_user():
    form = RegisterForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            return '<h1> User Already Exists! </h1>'
        else:
            user = User(username=form.username.data)
            user.hash_password(form.password.data)
            udb.session.add(user)
            udb.session.commit()
            return render_template('registerAccess.html')
    return flask.render_template('register.html', form=form)

@app.route('/register_page')
def register_page():
    form = RegisterForm()
    return render_template('register.html' , form=form)



@app.route('/api/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})

@app.route('/api/resource')
@auth.login_required
def get_resource():
    return render_template('index.php')
	
	
class listAll(Resource):
	def get(self):
		#Note: use.limit when looking for queries
		_items = db.tododb.find().sort('km', pymongo.ASCENDING)
		items = [item for item in _items]
		km = []
		open_times = []
		close_times = []
		for item in items:
			km.append(item["km"])
			open_times.append(item["open_time"])
			close_times.append(item["close_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["open_times"] = open_times
		JSONentry["close_times"] = close_times
		return JSONentry

class listOpenOnly(Resource):
	def get(self):
		_items = db.tododb.find().sort('km', pymongo.ASCENDING)
		items = [item for item in _items]
		km = []
		open_times = []
		for item in items:
			km.append(item["km"])
			open_times.append(item["open_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["open_times"] = open_times
		return JSONentry

class listCloseOnly(Resource):
	def get(self):
		_items = db.tododb.find().sort('km', pymongo.ASCENDING)
		items = [item for item in _items]
		km = []
		close_times = []
		for item in items:
			km.append(item["km"])
			close_times.append(item["close_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["close_times"] = close_times
		return JSONentry

class listAllJSON(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		km = []
		open_times = []
		close_times = []
		for item in items:
			km.append(item["km"])
			open_times.append(item["open_time"])
			close_times.append(item["close_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["open_times"] = open_times
		JSONentry["close_times"] = close_times
		return JSONentry

class listOpenOnlyJSON(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		km = []
		open_times = []
		for item in items:
			km.append(item["km"])
			open_times.append(item["open_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["open_times"] = open_times
		return JSONentry

class listCloseOnlyJSON(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		km = []
		close_times = []
		for item in items:
			km.append(item["km"])
			close_times.append(item["close_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["close_times"] = close_times
		return JSONentry

class listAllCSV(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		csv = "(openTime, CloseTime): "
		for item in items:
			csv += "({}, ".format(item["open_time"])
			csv += "{}), ".format(item["close_time"])
		return csv

class listOpenOnlyCSV(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		csv = "openTime: "
		for item in items:
			csv += "{}, ".format(item["open_time"])
		return csv

class listCloseOnlyCSV(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		csv = "CloseTime: "
		for item in items:
			csv += "{}, ".format(item["close_time"])
		return csv

"""
class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }

# Create routes
# Another way, without decorators
api.add_resource(Laptop, '/')
"""
api.add_resource(listAll, '/listAll')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listAllJSON, '/listAll/JSON')
api.add_resource(listOpenOnlyJSON, '/listOpenOnly/JSON')
api.add_resource(listCloseOnlyJSON, '/listCloseOnly/JSON')
api.add_resource(listAllCSV, "/listAll/CSV")
api.add_resource(listOpenOnlyCSV, "/listOpenOnly/CSV")
api.add_resource(listCloseOnlyCSV, "/listCloseOnly/CSV")
# Run the application
if __name__ == '__main__':
    if not os.path.exists('db.sqlite'):
        udb.create_all()
    app.run(host='0.0.0.0', port=80, debug=True)
