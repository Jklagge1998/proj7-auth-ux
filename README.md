# Project 7: Adding authentication and user interface to brevet time calculator service

## About
Author: Jackson Klagge

Email: Jklagge1998@gmail.com

Overview: The goal of this project was build a authentication protocol on top of project 6 with protect fields.

## Recap of Project 6 

Recall: We created the following three parts: 

* We designed RESTful services to expose what is stored in MongoDB. Specifically, we used the boilerplate given in DockerRestAPI folder, and created the following:

** "http://<host:port>/listAll" should return all open and close times in the database

** "http://<host:port>/listOpenOnly" should return open times only

** "http://<host:port>/listCloseOnly" should return close times only

* We also designed two different representations: one in csv and one in json. For the above, JSON should be the default representation. 

** "http://<host:port>/listAll/csv" should return all open and close times in CSV format

** "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format

** "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

** "http://<host:port>/listAll/json" should return all open and close times in JSON format

** "http://<host:port>/listOpenOnly/json" should return open times only in JSON format

** "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* We also added a query parameter to get top "k" open and close times. For examples, see below.

** "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 

** "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format

* We also designed consumer programs (e.g., in jQuery) to expose the services.

## Functionality I added

In this project, I added the following functionalities:

### Part 1: Authenticating the services 

- POST **/api/register**

Registers a new user. On success a status code 201 is returned. The body of the response contains a JSON object with the newly added user. A `Location` header contains the URI of the new user. On failure status code 400 (bad request) is returned. Note: The password is hashed before it is stored in the database. Once hashed, the original password is discarded. Your database should have three fields: id (unique index), username and password for storing the credentials.

- GET **/api/token**

Returns a token. This request must be authenticated using a HTTP Basic Authentication (see password.py for example). On success a JSON object is returned with a field `token` set to the authentication token for the user and a field `duration` set to the (approximate) number of seconds the token is valid. On failure status code 401 (unauthorized) is returned.

- GET **/RESOURCE-YOU-CREATED-IN-PROJECT-6**

Return a protected <resource>, which is basically what you created in project 6. This request must be authenticated using token-based authentication only (see testToken.py). HTTP password-based (basic) authentication is not allowed. On success a JSON object with data for the authenticated user is returned. On failure status code 401 (unauthorized) is returned.

### Part 2: User interface

I created frontend/UI for Brevet app using Flask-WTF and Flask-Login introduced in lectures. My frontend/UI should use the authentication that you created above. In addition to creating UI for basic authentication and token generation, I added three additional functionalities in my UI: (a) remember me, (b) logout, and (c) CSRF protection.

## How it works
To start the program do the following:

* In the DockerRESTAPI directory run the run.sh shell script. This will use docker commands to build and run our container.

There are 3 ports in this service:

* Port 5000: Exposes what's stored in our Mongo db (submitted brevet calculator entries).

* Port 5002: Our brevet time calculator table where the user will input distances in km or miles(range 0 - 1300 km).

* Port 5001: This is the main port of this project. It contains a user login/registration system where the user will need to log in to get to procted areas. This also is where our implentation of Authentication services go.

Port 5001 contains the following:

* Login page: This will be default page. If you already have an account log in using that. If not there is a link to get registered. If the user logs in successfully they will go to a success page in which they can logout, register with a different user, go get their token, or get the resource page (which should be what we exposed last project in port 5000). 

* Register page: The user will input a username and password (between 4 and 12 characters) to get registered. Upon registration the user will go to a success page in which they can follow a link to be logged in. 

## Project Notes

* For this project I decided to SQLAlchemy and sqlite for a user database. It should work fine, but while delevolping it would sometimes crash. I believe I resolved the issue, but if it doesn't usually rerunning your container does the trick.

* The protected sites (login required) only worked when I initally put a user in or after I rebuilt the contrainers. This might be due load user acting as a cookie for my device and I wasn't waiting enough time to expire. I also used auth.login_required before my protected links which were api/token and api/resource.

